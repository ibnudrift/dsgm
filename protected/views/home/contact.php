<section class="cover page-contact">
    <div class="container cont-fcs produk">
        <div class="cover-image">
            <!-- <img class="w-100 d-block" src="<?php echo $this->assetBaseurl; ?>ill-contacts.jpg" alt=""> -->
            <div class="centered wow fadeInUp">
              <p>
                <?php echo strtoupper(Tt::t('front', 'Contact')); ?>
              </p>
            </div>
        </div>
        <div class="row pt-3">
          <div class="col-40">
            <div class="breadcrumb wow fadeInUp">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">HOME</a></li>
                  <li class="breadcrumb-item"><a href="#"><?php echo strtoupper(Tt::t('front', 'Contact')); ?></a></li>
                </ol>
              </nav>
            </div>
          </div>
          <div class="col-20">
            <div class="back text-right wow fadeInUp">
              <a href="#"><?php echo strtoupper(Tt::t('front', 'BACK')); ?></a>
            </div>
          </div>
        </div>
        <hr class="cover">
    </div>
</section>

<section class="contact-sec-1">
  <div class="prelative container">
    <?php if (Yii::app()->language == 'en'): ?>
    <div class="row pt-5 wow fadeInUp">
      <div class="col-md-60">
        <div class="text1 mx-auto d-block text-center pb-4">
          <p>NEED MORE INFORMATION?</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text2 mx-auto d-block text-center pb-4">
          <p>We’re Here To Help You</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text3 mx-auto d-block text-center pb-4">
          <p>Contact us for any inquiries or product related information.</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text4 mx-auto d-block text-center pb-5">
          <p>Let our customer service and product specialist team study your request and then provide answer to all your questions about the right Calcium Carbonate product. We claim ourselves to be the best in understanding your needs and specifications on efficient calcium carbonate product applications for your business operationals. </p>
        </div>
      </div>
    </div>
    <?php else: ?>
      <div class="row pt-5 wow fadeInUp">
      <div class="col-md-60">
        <div class="text1 mx-auto d-block text-center pb-4">
          <p>BUTUH INFORMASI LEBIH?</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text2 mx-auto d-block text-center pb-4">
          <p>Kami Di Sini Untuk Membantu Anda</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text3 mx-auto d-block text-center pb-4">
          <p>Hubungi kami untuk setiap pertanyaan atau informasi terkait produk.</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text4 mx-auto d-block text-center pb-5">
          <p>Biarkan tim layanan pelanggan dan spesialis produk kami mempelajari permintaan Anda dan kemudian memberikan jawaban untuk semua pertanyaan Anda tentang produk Kalsium Karbonat yang tepat. Kami mengklaim diri sebagai yang terbaik dalam memahami kebutuhan dan spesifikasi Anda pada aplikasi produk kalsium karbonat yang efisien untuk operasional bisnis Anda.</p>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <div class="row wow fadeInUp">
      <div class="col-md-60">
        <div class="factory mx-auto d-block text-center pb-4 pt-3 ">
          <?php if (Yii::app()->language == 'en'): ?>
          <p>FACTORY & HEAD OFFICE</p>
          <?php else: ?>
          <p>Pabrik & Kantor di Indonesia</p>
          <?php endif; ?>
        </div>
        <div class="alamat mx-auto d-block text-center pb-4 ">
          <div class="title">
            <p>SIDOARJO</p>
          </div>
          <div class="isi">
            <p>Jl. Raya Tebel No.50-A, Tebel <br>
              Gedangan, Sidoarjo, Jawa Timur 61254 - INDONESIA</p>
          </div>
          <div class="view">
            <?php if (Yii::app()->language == 'en'): ?>
            <a href="https://goo.gl/maps/4FudkGBXZCGxCsqF8">
              <p>(View On Google Map)</p>
            </a>
            <?php else: ?>
            <p>(Lihat di Google Map)</p>
          <?php endif; ?>
          </div>
        </div>
        <div class="telepon mx-auto d-block text-center pb-4 ">
          <div class="title">
            <?php if (Yii::app()->language == 'en'): ?>
            <p>Telephone.</p>
            <?php else: ?>
            <p>Telepon.</p>
            <?php endif; ?>
          </div>
          <div class="nomor">
            <p>+62 31 8913030</p>
          </div>
        </div>
        <div class="telepon mx-auto d-block text-center pb-4 ">
          <div class="title">
            <?php if (Yii::app()->language == 'en'): ?>
            <p>Sales Whatsapp.</p>
            <?php else: ?>
            <p>Sales Whatsapp.</p>
            <?php endif; ?>
          </div>
          <div class="nomor">
            <!-- <a href="https://wa.me/628883205551"> -->
              <p><span><a href="https://wa.me/628883205551">0888-3205-551</a></span> dan <span><a href="https://wa.me/6281217672188">0812-1767-2188</a></span></p>
            <!-- </a> -->
          </div>
        </div>
        <!-- <div class="email mx-auto d-block text-center pb-4 ">
          <div class="title">
            <p>Email.</p>
          </div>
          <div class="nomor">
            <p><a href="mailto:info@dwiselogirimas.com">info@dwiselogirimas.com</a></p>
          </div>
        </div> -->
      </div>
    </div>
    <div class="row pt-5 wow fadeInUp">
      <div class="col-md-60">
        <div class="factory mx-auto d-block text-center pb-4 pt-3 ">
          <?php if (Yii::app()->language == 'en'): ?>
          <p>BRANCH OFFICE</p>
          <?php else: ?>
          <p>Pabrik & Kantor di Indonesia</p>
          <?php endif; ?>
        </div>
        <div class="alamat mx-auto d-block text-center pb-4 ">
          <div class="title">
            <p>Jakarta Barat</p>
          </div>
          <div class="isi">
            <p>Jl Daan Mogot <br>
            Komplek Pertokoan Green Garden Blok A7 / 16</p>
          </div>
          <!-- <div class="view">
            <?php if (Yii::app()->language == 'en'): ?>
            <p>(View On Google Map)</p>
            <?php else: ?>
            <p>(Lihat di Google Map)</p>
          <?php endif; ?>
          </div> -->
        </div>
        <div class="telepon mx-auto d-block text-center pb-4 ">
          <div class="title">
            <?php if (Yii::app()->language == 'en'): ?>
            <p>Telephone.</p>
            <?php else: ?>
            <p>Telepon.</p>
            <?php endif; ?>
          </div>
          <div class="nomor">
            <p>021-5809710</p>
          </div>
        </div>
        <div class="telepon mx-auto d-block text-center pb-4 ">
          <div class="title">
            <?php if (Yii::app()->language == 'en'): ?>
            <p>Fax.</p>
            <?php else: ?>
            <p>Fax.</p>
            <?php endif; ?>
          </div>
          <div class="nomor">
            <p>021-5809710</p>
          </div>
        </div>
        <!-- <div class="email mx-auto d-block text-center pb-4 ">
          <div class="title">
            <p>Email.</p>
          </div>
          <div class="nomor">
            <p><a href="mailto:info@dwiselogirimas.com">info@dwiselogirimas.com</a></p>
          </div>
        </div> -->
      </div>
    </div>

  </div>
  <div class="pb-5"></div>
  <div class="pb-2"></div>
</section>

<section class="form-contact">
  <div class="prelative container">
    <div class="py-5"></div>
    <!-- <div class="py-5"></div> -->
    <div class="title wow fadeInUp">
      <p>Online Contact Form</p>
    </div>
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                'type'=>'',
                'enableAjaxValidation'=>false,
                'clientOptions'=>array(
                    'validateOnSubmit'=>false,
                ),
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data',
                    'class'=>'py-5',
                ),
            )); ?>
    <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>  
    <?php if(Yii::app()->user->hasFlash('success')): ?>
        <?php $this->widget('bootstrap.widgets.TbAlert', array(
            'alerts'=>array('success'),
        )); ?>
    <?php endif; ?>
      <div class="row default">
        <div class="col-md-15 col-sm-15">
          <div class="form-group">
            <!-- <label for="exampleInputName">Nama</label> --> 
            <?php echo $form->textField($model, 'name', array('class'=>'form-control', 'placeholder'=>'Name')); ?>   
          </div>
        </div>
        <div class="col-md-15 col-sm-15">
          
          <div class="form-group">
            <!-- <label for="exampleInputCompany">Perusahaan</label> -->
            <?php echo $form->textField($model, 'company', array('class'=>'form-control', 'placeholder'=>'Company')); ?>
              
            </div>
        </div>

        <div class="col-md-15 col-sm-15">
          <div class="form-group">
            <!-- <label for="exampleInputEmail">Email</label> -->
            <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'placeholder'=>'Email')); ?>
          </div>
        </div>
        <div class="col-md-15 col-sm-15">
          <div class="form-group">
          <!-- <label for="exampleInputPhone">Telepon</label> -->
          <?php echo $form->textField($model, 'phone', array('class'=>'form-control', 'placeholder'=>'Phone')); ?>
          </div>
        </div>
      </div>

      <div class="row default no-gutters">
      <div class="col-md-60">
        <div class="form-group">
      </div>
      <div class="clear"></div>

      <div class="row default text-area">
        <div class="col-md-60">
          <?php echo $form->textArea($model, 'body', array('class'=>'form-control', 'placeholder'=> 'Notes / Messages')); ?>
        </div>
      </div>

      <div class="row default wow fadeInUp">
        <div class="col-md-60">
          <div class="py-3"></div>
          <div class="mx-auto d-block picts_captcha">
            <div class="g-recaptcha" data-sitekey="6LeAzqgUAAAAAHQABMAYvPm5BOrhCKm6WzaQHl6S"></div>
          </div>
          <div class="submit">
            <button type="submit" class="">Submit</button>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-60">
        <div class="row default">
          <div class="col-md-60 col-sm-60">
          </div>
          <div class="col-md-60 col-sm-60 hide-pc">
            <div class="text-right submit">
            <a href="#">Submit</a>
            </div>
          </div>
          <div class="col-md-60 hide-mobile">
            <div class="text-center submit">
            <a href="#">Submit</a>
            </div>
          </div>
                        
          <div class="py-5 hide-mobile"></div>
          <div class="py-5 hide-mobile"></div>
        </div>

      </div> -->
      </div>
    <?php $this->endWidget(); ?>
  </div>
  <div class="pb-5"></div>
</section>

<style type="text/css">
  section.contact-sec-1 .email .nomor p a{
    font-size: 15px;
    font-weight: 500;
    color: #fff;
  }

  section.form-contact form .submit button{
    background: #d9d9d9;
    border-radius: 20px;
    cursor: pointer;
    border: 0;
    padding: 10px 70px;
    color: #434343;
    font-weight: 500;
    text-transform: uppercase;
    font-size: 18px;
    text-decoration: none;
  }
  .picts_captcha .g-recaptcha > div{
    display: block;
    margin: 0 auto;
  }
</style>

<script src='https://www.google.com/recaptcha/api.js'></script>