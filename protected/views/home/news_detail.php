<section class="block-breadcrumb">
  <div class="prelative container2">
    <div class="row">
      <div class="col-40">
        <div class="breadcrumb">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=> Yii::app()->language)); ?>">HOME</a></li>
              <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/news', 'lang'=> Yii::app()->language)); ?>"><?php echo strtoupper( Tt::t('front', 'News & Articles') ); ?></a></li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-20">
        <div class="back text-right">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/news', 'lang'=> Yii::app()->language)); ?>"><?php echo strtoupper( Tt::t('front', 'BACK') ); ?></a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="title-breadcrumb2">
  <div class="container-news">
    <div class="kategori mx-auto d-block text-center">
      <p>CORPORATE NEWS</p>
    </div>
    <div class="title mx-auto d-block text-center">
      <h3><?php echo $data->description->title ?></h3>
    </div>
    <div class="tanggal mx-auto d-block text-center">
      <p>
        <?php echo date('d F Y', strtotime($data->date_input)); ?>
      </p>
    </div> 
  </div>
</section>

<div class="pt-4"></div>
<div class="pt-5"></div>
<section class="news-detail-sec-1">
  <div class="prelative container">
    <div class="row">
      <div class="col-md-60">
        <div class="box-content">
          <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.'/images/blog/'. $data->image ?>" alt="">
          <div class="content pt-4 mt-3">
            <?php echo $data->description->content ?>
            <div class="clear"></div>
          </div>
        </div>
      </div>

      <div class="col-md-60">
        <div class="latest">
        <div class="pt-5"></div>
        <div class="pt-3"></div>
          <p>Latest News & Articles About Calcium Carbonate</p>
        </div>
        <div class="pb-5 hide-pc"></div>
        <div class="pb-3"></div>
      </div>

      <?php foreach ($other->getData() as $key => $value): ?>        
      <div class="col-md-20">
        <div class="box-news">
          <div class="tanggal">
            <p><?php echo date('d F Y', strtotime($value->date_input)); ?></p>
          </div>
          <div class="title py-3">
            <a href="<?php echo CHtml::normalizeUrl(array('/home/news_detail', 'id'=>$value->id, 'lang'=>Yii::app()->language)); ?>">
              <h2><?php echo $value->description->title ?></h2>
            </a>
          </div>
          <div class="content">
            <p><?php echo substr(strip_tags($value->description->content), 0, 150); ?></p>
          </div>
        </div>
      </div>
      <?php endforeach; ?> 

    </div>
  </div>
  <div class="pb-5 hide-pc"></div>
  <div class="pb-5 hide-pc"></div>
  <div class="pb-5"></div>
</section>

<section class="next-prev">
  <div class="prelative container mx-auto d-block text-center">
    <ul>
      <li>
        <a href="#">News & Article Index</a>
      </li>
      <li class="garis-awal">
        <div class="garis"></div>
      </li>
      <li>
        <a href="#">Next Article</a>
      </li>
    </ul>
  </div>
</section>

<!-- <div class="pb-3"></div> -->
