<section class="cover page-career">
    <div class="container cont-fcs produk">
        <div class="cover-image">
            <!-- <img class="w-100 d-block" src="<?php echo $this->assetBaseurl; ?>ill-career.jpg" alt=""> -->
            <div class="centered wow fadeInUp">
              <p>
                <?php echo Tt::t('front', 'Career'); ?>
              </p>
            </div>
        </div>
        <div class="row pt-3">
          <div class="col-40">
            <div class="breadcrumb wow fadeInUp">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">HOME</a></li>
                  <li class="breadcrumb-item"><a href="#"><?php echo Tt::t('front', 'Career'); ?></a></li>
                </ol>
              </nav>
            </div>
          </div>
          <div class="col-20">
            <div class="back text-right wow fadeInUp">
              <a href="#"><?php echo Tt::t('front', 'BACK'); ?></a>
            </div>
          </div>
        </div>
        <hr class="cover">
    </div>
</section>

<section class="career-sec-1">
  <div class="prelative container">
    <?php if (Yii::app()->language == 'en'): ?>
    <div class="row wow fadeInUp">
      <div class="col-md-60">
        <div class="text1 mx-auto text-center pt-5">
          <p>A BRIGHTER FUTURE</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text2 mx-auto text-center pt-4">
          <p>Are You Up To The Challenge?</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text3 mx-auto text-center pt-4">
          <p>At PT. Dwi Selo Giri Mas, you will become a part of a growing national company with a culture that allows you to bring out the best in you.</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text4 mx-auto text-center pt-4">
          <p>PT. Dwi Selo Giri Mas will offer you a culturally diverse and collaborative work experience. You will get to work very closely with some of the smartest people in the indonesian chemical industry from across the nation. You will be given the opportunity to learn, explore and fulfil your career goals. Let us know how you can create value for our company. </p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text5 mx-auto text-center pt-4">
          <p>Send your complete resume to: <br> <a href="#">info@dwiselogirimas.com</a> </p>
        </div>
      </div>
    </div>
    <?php else: ?>
    <div class="row wow fadeInUp">
      <div class="col-md-60">
        <div class="text1 mx-auto text-center pt-5">
          <p>MASA DEPAN YANG LEBIH BAIK</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text2 mx-auto text-center pt-4">
          <p>Apakah Anda Siap Menantang?</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text3 mx-auto text-center pt-4">
          <p>Di PT. Dwi Selo Giri Mas, Anda akan menjadi bagian dari perusahaan nasional yang berkembang dengan budaya yang memungkinkan Anda mengeluarkan yang terbaik dari diri Anda.</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text4 mx-auto text-center pt-4">
          <p>PT. Dwi Selo Giri Mas akan menawarkan Anda pengalaman kerja yang beragam secara budaya dan kolaboratif. Anda akan bekerja dengan sangat erat dengan beberapa orang terpintar di industri kimia Indonesia dari seluruh negara. Anda akan diberi kesempatan untuk belajar, mengeksplorasi, dan memenuhi tujuan karier Anda. Beri tahu kami bagaimana Anda dapat menciptakan nilai bagi perusahaan kami.</p>
        </div>
      </div>
      <div class="col-md-60">
        <div class="text5 mx-auto text-center pt-4">
          <p>Kirim resume lengkap Anda ke: <br> <a href="#">info@dwiselogirimas.com</a> </p>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <div class="pb-5"> </div>
    <div class="pb-5"></div>
  </div>
</section>

<section class="form-resume">
  <div class="prelative container">
    <div class="py-5"></div>
    <!-- <div class="py-5"></div> -->
    <div class="title wow fadeInUp">
      <p>Send Your Complete Resume Form</p>
    </div>
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                'type'=>'',
                'enableAjaxValidation'=>false,
                'clientOptions'=>array(
                    'validateOnSubmit'=>false,
                ),
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data',
                    'class'=>'py-5 wow fadeInUp',
                ),
            )); ?>
    <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>  
      <div class="row default">
        <div class="col-md-15 col-sm-15">
          <div class="form-group">
            <!-- <label for="exampleInputName">Nama</label> -->
            <?php echo $form->textField($model, 'name', array('class'=>'form-control', 'placeholder'=>'Name')); ?>        
          </div>
        </div>
        <div class="col-md-15 col-sm-15">
          
          <div class="form-group">
            <!-- <label for="exampleInputCompany">Perusahaan</label> -->
            <?php echo $form->textField($model, 'position', array('class'=>'form-control', 'placeholder'=>'Position')); ?>        
          </div>
        </div>

        <div class="col-md-15 col-sm-15">
          <div class="form-group">
            <!-- <label for="exampleInputEmail">Email</label> -->
            <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'placeholder'=>'Email')); ?>        
          </div>
        </div>
        <div class="col-md-15 col-sm-15">
          <div class="form-group">
          <!-- <label for="exampleInputPhone">Telepon</label> -->
          <?php echo $form->textField($model, 'phone', array('class'=>'form-control', 'placeholder'=>'Phone')); ?>        
        </div>
        </div>
      </div>

      <!-- <div class="row default no-gutters">
      <div class="col-md-60">
        <div class="form-group">
      </div>
      <div class="clear"></div>

      <div class="row default text-area">
        <div class="col-md-60">
          <textarea name="" id="" cols="" rows="" placeholder="Notes / Messages"></textarea>
        </div>
      </div> -->


      <div class="row default wow fadeInUp">
        <div class="col-md-60">
          <div class="submit resume">
            <a style="display:block;background:transparent;" href="#">Attach Your Resume</a>
            <?php echo $form->fileField($model, 'files', array('required'=>'required', 'placeholder'=>'Attach Your Resume')); ?>
            <p>Maximum limit is 2 Megabytes</p>
          </div>
        </div>
      </div>

      <div class="row default wow fadeInUp">
        <div class="col-md-60">
          <div class="py-3"></div>
          <div class="mx-auto d-block picts_captcha">
            <div class="g-recaptcha" data-sitekey="6LeAzqgUAAAAAHQABMAYvPm5BOrhCKm6WzaQHl6S"></div>
          </div>
          <div class="submit">
            <button type="submit">SUBMIT</button>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-60">
        <div class="row default">
          <div class="col-md-60 col-sm-60">
          </div>
          <div class="col-md-60 col-sm-60 hide-pc">
            <div class="text-right submit">
            <a href="#">Submit</a>
            </div>
          </div>
          <div class="col-md-60 hide-mobile">
            <div class="text-center submit">
            <a href="#">Submit</a>
            </div>
          </div>
                        
          <div class="py-5 hide-mobile"></div>
          <div class="py-5 hide-mobile"></div>
        </div>

      </div> -->
      </div>
    <?php $this->endWidget(); ?>
  </div>
  <div class="pb-5"></div>
</section>
<style type="text/css">
  section.form-resume form .submit button {
    background: #d9d9d9;
    border-radius: 20px;
    cursor: pointer;
    border: 0;
    padding: 10px 70px;
    color: #434343;
    font-weight: 500;
    font-size: 18px;
    text-decoration: none;
}
.picts_captcha .g-recaptcha > div{
    display: block;
    margin: 0 auto;
  }
   input[type='file'] {
    background: #d9d9d9;
    border-radius: 20px;
    cursor: pointer;
    border: 0;
    padding: 10px 40px;
    color: #434343;
    font-weight: 500;
    font-size: 12px;
    text-decoration: none;
  }
</style>
<script src='https://www.google.com/recaptcha/api.js'></script>