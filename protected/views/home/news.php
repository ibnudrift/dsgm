<section class="block-breadcrumb">
  <div class="prelative container2">
    <div class="row">
      <div class="col-40">
        <div class="breadcrumb wow fadeInUp">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">HOME</a></li>
              <li class="breadcrumb-item"><a href="#"><?php echo strtoupper( Tt::t('front', 'Events') ); ?></a></li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-20">
        <div class="back text-right wow fadeInUp">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><?php echo strtoupper( Tt::t('front', 'BACK') ); ?></a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="title-breadcrumb">
  <div class="title mx-auto d-block text-center wow fadeInUp">
    <h3><?php echo ucwords( Tt::t('front', 'Events') ); ?></h3>
  </div>
</section>

<!-- <section class="pilihan hide-mobile">
  <div class="prelative container text-center d-block mx-auto">
    <ul>
      <li class="active"><a href="#">
        ALL ARTICLES
          </a> 
      </li>
      <li class="garis-awal"><div class="garis"></div></li>
      <li><a href="#">
        CORPORATE NEWS
          </a> 
      </li>
      <li class="garis-awal"><div class="garis"></div></li>
      <li><a href="#">
        CALCIUM CARBONATE RELATED ARTICLES
          </a> 
      </li>
        
    </ul>
  </div>
</section> -->

<!-- <section class="pilihan hide-pc">
  <div class="prelative container text-center d-block mx-auto">
    <ul>
      <li class="active"><a href="#">
        ALL ARTICLES
          </a> 
      </li>
      <li class="garis-awal"><div class="garis"></div></li>
      <li><a href="#">
        CORPORATE NEWS
          </a> 
      </li>
      <li class="garis-awal"><div class="garis"></div></li>
      <li><a href="#">
        CALCIUM CARBONATE RELATED ARTICLES
          </a> 
      </li>
        
    </ul>
  </div>
</section> -->

<section class="news-sec-1">
  <div class="prelative container news">
    <div class="row">
      
      <?php foreach ($data->getData() as $key => $value): ?>        
        <div class="col-md-20 wow fadeInUp">
          <div class="box-news">
            <div class="tanggal">
              <p><?php echo date('d F Y', strtotime($value->date_input)); ?></p>
            </div>
            <div class="title py-3">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/news_detail', 'id'=>$value->id, 'lang'=>Yii::app()->language)); ?>">
                <h2><?php echo $value->description->title ?></h2>
              </a>
            </div>
            <div class="content">
              <p><?php echo substr(strip_tags($value->description->content), 0, 150); ?></p>
            </div>
          </div>
        </div>
        <?php endforeach ?>

      </div>

  </div>
  <div class="py-4"></div>
  <div class="pb-5"></div>
  <hr>
</section>