<section class="cover">
    <div class="container cont-fcs produk">
        <div class="cover-image" style="background-image: url('<?php echo $this->assetBaseurl; ?>Layer-311.jpg'); background-position: center;">
            <!-- <img class="w-100 d-block" src="<?php echo $this->assetBaseurl; ?>ill-process.jpg" alt=""> -->
            <div class="centered wow fadeInUp">
              <p>
                <?php echo strtoupper( Tt::t('front', 'Products') ); ?>
              </p>
            </div>
        </div>
        <div class="row pt-3">
          <div class="col-40">
            <div class="breadcrumb wow fadeInUp">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">HOME</a></li>
                  <li class="breadcrumb-item"><a href="#"><?php echo strtoupper( Tt::t('front', 'Products') ); ?></a></li>
                </ol>
              </nav>
            </div>
          </div>
          <div class="col-20">
            <div class="back text-right wow fadeInUp">
              <a href="#"><?php echo strtoupper( Tt::t('front', 'BACK') ); ?></a>
            </div>
          </div>
        </div>
        <hr class="cover">
    </div>
</section>
<section class="cover-above">
    <div class="prelative container">
        <div class="row pt-5 wow fadeInUp">
            <?php if (Yii::app()->language == 'en'): ?>
            <div class="col-md-60">
                <div class="caption">
                    <div class="text1 mx-auto d-block text-center pb-4">
                        <p>our product as part of your operational</p>
                    </div>
                </div>
            </div>
            <div class="col-md-60">
                <div class="caption">
                    <div class="text2 mx-auto d-block text-center pb-4">
                        <p>Our Calcium Carbonate With You In Mind</p>
                    </div>
                </div>
            </div>
            <div class="col-md-60">
                <div class="caption">
                    <div class="text3 mx-auto d-block text-center pb-4">
                        <p>We take pride in providing high quality calcium carbonate products to our market in Surabaya, Jakarta and throuhout Indonesia. We will meet your specification and we will ensure a steady production flow by maintaining our stock priority for your long-term.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-60">
                <div class="caption">
                    <div class="text4 mx-auto d-block text-center pb-4">
                        <p>PT. DSGM (Dwi Selo Giri Mas) is one of the leading Carbonate manufacture for Ground Calcium Carbonate (GCC) in Indonesia. We obtain the raw material from our mining site of calcite lime stone in Central Java, Indonesia. The lime stone is then grinded into fine powder and classified precisely to attain the precise fineness and quality needed by our customers.</p> <br>
                        <p>Calcium Carbonate is an essential material for the producing of paint industries, chemical industries, ceramics industries, plastic industries, paper industries and so much more. A quality calcium carbonate that meets the requirements will ensure the smooth operation and production of our customers factory. As an important and integral part of the production, calcium carbonate came at a very low price - therefore there are no reasons or options for our customers to choose a lower grade of calcium carbonate. “With you in mind” as our everyday spirit and commitment, we produce the best product possible to guarantee your success.</p>
                    </div>
                </div>
            </div>
            <?php else: ?>
            <div class="col-md-60">
                <div class="caption">
                    <div class="text1 mx-auto d-block text-center pb-4">
                        <p>produk kami sebagai bagian dari operasional Anda</p>
                    </div>
                </div>
            </div>
            <div class="col-md-60">
                <div class="caption">
                    <div class="text2 mx-auto d-block text-center pb-4">
                        <p>Kalsium Karbonat Kami Dengan Anda Dalam Pikiran</p>
                    </div>
                </div>
            </div>
            <div class="col-md-60">
                <div class="caption">
                    <div class="text3 mx-auto d-block text-center pb-4">
                        <p>Kami bangga menyediakan produk kalsium karbonat berkualitas tinggi untuk pasar kami di Surabaya, Jakarta dan di seluruh Indonesia. Kami akan memenuhi spesifikasi Anda dan kami akan memastikan aliran produksi yang stabil dengan mempertahankan prioritas stok kami untuk jangka panjang Anda.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-60">
                <div class="caption">
                    <div class="text4 mx-auto d-block text-center pb-4">
                        <p>Kalsium Karbonat adalah bahan penting untuk memproduksi industri cat, industri kimia, industri keramik, industri plastik, industri kertas dan banyak lagi. Kalsium karbonat berkualitas yang memenuhi persyaratan akan memastikan kelancaran operasi dan produksi pabrik pelanggan kami. Sebagai bagian penting dan integral dari produksi, kalsium karbonat dijual dengan harga yang sangat rendah - oleh karena itu tidak ada alasan atau pilihan bagi pelanggan kami untuk memilih kadar kalsium karbonat yang lebih rendah. “Dengan pertimbangan Anda” sebagai semangat dan komitmen kami sehari-hari, kami menghasilkan produk terbaik untuk menjamin kesuksesan Anda.</p>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="py-5"></div>
    <div class="py-5"></div>
    <div class="py-4"></div>
    <!-- <div class="pb-5"></div> -->
</section>

<section class="produk-sec-1 outersec_prd_2">
    <div class="prelative container pt-5">
        <div class="picts_mid wow fadeInUp">
            <img src="<?php echo $this->assetBaseurl; ?>in-mid_salts.jpg" alt="" class="img img-fluid d-block mx-auto">
        </div>
        <div class="title pt-5 mx-auto d-block text-center wow fadeInUp">
            <?php if (Yii::app()->language == 'en'): ?>
            <h3>DWI SELO GIRIMAS’ CALCIUM CARBONATES ARE IMPORTANT <br> FOR INDUSTRIAL USAGE AS SHOWN IN THE DIAGRAM BELOW</h3>
            <div class="py-1"></div>
            <p>Scroll down to find more information.</p>
            <div class="py-1"></div>
            <!-- <a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>" class="btn btn-link btns_tcontact_pr">CONTACT US</a> -->
            <?php else: ?>
            <h3>KAMI MENGHASILKAN KARBONAT KALSIUM UNTUK PERSYARATAN ANDA</h3>
            <div class="py-1"></div>
            <p>Silakan tanyakan - konsultan kami akan membahas spesifikasi terperinci Anda (kemurnian, mesh, dll).</p>
            <div class="py-1"></div>
            <!-- <a href="<?php // echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>" class="btn btn-link btns_tcontact_pr">HUBUNGI KAMI</a> -->
            <?php endif; ?>
        </div>
        <div class="row wow fadeInUp">
            <div class="col-md-60">
                <div class="pt-5"></div>
                <div class="pt-4"></div>
                <div class="image">
                    <img class="mx-auto d-block" src="<?php echo $this->assetBaseurl; ?>diagram.png" alt="diagram natriaum carbonate" usemap="#Map">
                    <map name="Map" id="Map">
                        <area class="toscroll" data-id="paint-coating" title="paint-coating" href="#paint-coating" shape="poly" coords="6,233,159,262,174,330,38,408" />
                        <area class="toscroll" data-id="fishery" title="fishery" href="#fishery" shape="poly" coords="179,337,232,374,181,520,55,418" />
                        <area class="toscroll" data-id="ceramics-glass" title="ceramics-glass" href="#ceramics-glass" shape="poly" coords="241,383,302,386,358,522,192,522" />
                        <area class="toscroll" data-id="fertilizer" title="rubber" href="#rubber" shape="poly" coords="315,375,367,338,499,418,367,523" />
                        <area class="toscroll" data-id="rubber" title="fertilizer" href="#fertilizer" shape="poly" coords="368,328,505,402,540,238,388,261" />
                        <area class="toscroll" data-id="adhesives" title="adhesives" href="#adhesives" shape="poly" coords="382,251,538,220,452,72,354,188" />
                        <area class="toscroll" data-id="contruction" title="contruction" href="#contruction" shape="poly" coords="343,182,443,63,275,4,279,152" />
                        <area class="toscroll" data-id="animal-feed" title="animal-feed" href="#animal-feed" shape="poly" coords="100,68,264,6,268,158,200,187" />
                        <area class="toscroll" data-id="water-treatment" title="water-treatment" href="#water-treatment" shape="poly" coords="94,75,193,192,159,250,11,226" />
                    </map>
                </div>
                <div class="pb-4"></div>
            </div>
            <div class="col-md-60">
                <div class="content" id="paint-coating">
                    <?php if (Yii::app()->language == 'en'): ?>
                    <div class="title-content">
                        <p>Paints & Coatings</p>
                    </div>
                    <div class="isinya">
                        <p>Calcium carbonate enhance coatings performance as known as functional filler. Filler quality can improve decorative paints opacity, brightness, reflectance, workability and more. As well as on industrial coating, filler selection can make huge difference impact on gloss, viscosity and durability.</p>
                    </div>
                    <?php else: ?>
                    <div class="title-content">
                        <p>Cat & Pelapis</p>
                    </div>
                    <div class="isinya">
                        <p>Kalsium karbonat meningkatkan kinerja pelapisan yang dikenal sebagai pengisi fungsional. Kualitas pengisi dapat meningkatkan opacity cat dekoratif, kecerahan, reflektansi, kemampuan kerja dan banyak lagi. Seperti halnya pada pelapis industri, pemilihan pengisi dapat membuat dampak besar perbedaan pada gloss, viskositas dan daya tahan.</p>
                    </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="col-md-60">
                <div class="content" id="adhesives">
                    <?php if (Yii::app()->language == 'en'): ?>
                    <div class="title-content">
                        <p>Adhesives & Sealants</p>
                    </div>
                    <div class="isinya">
                        <p>The basic material in many adhesives and sealants are Calcium Carbonate, which are impoving rheologi, bond strength, and reducing water demand. </p>
                    </div>
                        
                    <?php else: ?>
                    <div class="title-content">
                        <p>Perekat & Sealant</p>
                    </div>
                    <div class="isinya">
                        <p>Bahan dasar dalam banyak perekat dan sealant adalah Kalsium Karbonat, yang meningkatkan reologi, kekuatan ikatan, dan mengurangi permintaan air.</p>
                    </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="col-md-60">
                <div class="content" id="ceramics-glass">
                    <?php if (Yii::app()->language == 'en'): ?>
                    <div class="title-content">
                        <p>Glass & Ceramics</p>
                    </div>
                    <div class="isinya">
                        <p>Glass are formed from certain types of rocks melt in high temperature and then cool and solidify rapidly. The function of calcium carbonate in glass production for stabilizer, modifies the viscosity and increases the durability. Other usage of calcium carbonate is reduce cost of ceramic production, because calcium carbonate is an economical source of calcium oxide which is needed as a melting agent that can improve mechanical and chemical strength and reduces shrinkage.  </p>
                    </div>    
                    <?php else: ?>
                    <div class="title-content">
                        <p>Kaca & Keramik</p>
                    </div>
                    <div class="isinya">
                        <p>Kaca terbentuk dari batuan jenis tertentu yang meleleh dalam suhu tinggi dan kemudian dingin dan mengeras dengan cepat. Fungsi kalsium karbonat dalam produksi kaca untuk stabilizer, memodifikasi viskositas dan meningkatkan daya tahan. Penggunaan lain kalsium karbonat adalah mengurangi biaya produksi keramik, karena kalsium karbonat merupakan sumber kalsium oksida yang ekonomis yang diperlukan sebagai zat leleh yang dapat meningkatkan kekuatan mekanik dan kimia serta mengurangi penyusutan.</p>
                    </div>
                    <?php endif ?>
                    
                </div>
            </div>
            <div class="col-md-60">
                <div class="content" id="contruction">
                    <?php if (Yii::app()->language == 'en'): ?>
                    <div class="title-content">
                        <p>Construction</p>
                    </div>
                    <div class="isinya">
                        <p>Calcium carbonate is well known for cemen based products, such as asphalt, roofing, tiles, and bricks. Calcium carbonate function in construction as stabilizer, accelerate the hydration of the cement matrix, increasing strength and durability. </p>
                    </div>
                    <?php else: ?>
                    <div class="title-content">
                        <p>Konstruksi</p>
                    </div>
                    <div class="isinya">
                        <p>Kalsium karbonat terkenal dengan produk berbasis semen, seperti aspal, atap, ubin, dan batu bata. Fungsi kalsium karbonat dalam konstruksi sebagai penstabil, mempercepat hidrasi matriks semen, meningkatkan kekuatan dan daya tahan.</p>
                    </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="col-md-60">
                <div class="content" id="rubber">
                    <?php if (Yii::app()->language == 'en'): ?>
                    <div class="title-content">
                        <p>Rubber Industries</p>
                    </div>
                    <div class="isinya">
                        <p>With good selection of calcium carbonate grade, can reduce cost of rubber production and improve mixing effect, and mold release.</p>
                    </div>
                    <?php else: ?>
                    <div class="title-content">
                        <p>Industri Karet</p>
                    </div>
                    <div class="isinya">
                        <p>Dengan pemilihan tingkat kalsium karbonat yang baik, dapat mengurangi biaya produksi karet dan meningkatkan efek pencampuran, dan pelepasan cetakan.</p>
                    </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="col-md-60">
                <div class="content" id="water-treatment">
                    <?php if (Yii::app()->language == 'en'): ?>
                    <div class="title-content">
                        <p>Water Treatment</p>
                    </div>
                    <div class="isinya">
                        <p>In order to increase the quality of natural or treated (e.g. desalinated) water to a non-corrosive level or to a level acceptable for human consumption, needs to adjust the pH value through neutralization, (re)mineralize the water through adding (dissolving) the required amount of calcium, magnesium and carbonate minerals or reduce the water hardness (decarbonization). An unsufficient level of calcium carbonate in treated water makes it corrosive and will cause equipment and structures to deteriorate, often while not fulfilling drinking water quality requirements such as those recommended by the WHO (World Health Organization). A supersaturated solution will likely precipitate calcium carbonate, causing scale, reduced efficiency and eventually leading to system failure.</p>
                    </div>
                    <?php else: ?>
                    <div class="title-content">
                        <p>Pengolahan air</p>
                    </div>
                    <div class="isinya">
                        <p>Untuk meningkatkan kualitas air alami atau air olahan (mis. Desalinasi) ke tingkat non-korosif atau ke tingkat yang dapat diterima untuk konsumsi manusia, perlu menyesuaikan nilai pH melalui netralisasi, (kembali) mengisolasi air dengan menambahkan (melarutkan) jumlah yang dibutuhkan kalsium, magnesium dan mineral karbonat atau mengurangi kesadahan air (dekarbonisasi). Tingkat kalsium karbonat yang tidak mencukupi dalam air olahan membuatnya korosif dan akan menyebabkan peralatan dan struktur memburuk, seringkali tanpa memenuhi persyaratan kualitas air minum seperti yang direkomendasikan oleh WHO (World Health Organization). Solusi jenuh mungkin akan mengendapkan kalsium karbonat, menyebabkan skala, mengurangi efisiensi dan akhirnya menyebabkan kegagalan sistem.</p>
                    </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="col-md-60">
                <div class="content" id="fertilizer">
                    <?php if (Yii::app()->language == 'en'): ?>
                    <div class="title-content">
                        <p>Fertilizers</p>
                    </div>
                    <div class="isinya">
                        <p>Calcium and magnesium fertilizers were known used for fertilizers. Fertilizers help to improves soils, plant nutrition and plant protection.</p>
                    </div>
                    <?php else: ?>
                    <div class="title-content">
                        <p>Pupuk</p>
                    </div>
                    <div class="isinya">
                        <p>Pupuk kalsium dan magnesium dikenal digunakan untuk pupuk. Pupuk membantu memperbaiki tanah, nutrisi tanaman dan perlindungan tanaman.</p>
                    </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="col-md-60">
                <div class="content" id="animal-feed">
                    <?php if (Yii::app()->language == 'en'): ?>
                    <div class="title-content">
                        <p>Animal Feed</p>
                    </div>
                    <div class="isinya">
                        <p>Calcium is an essential mixing element for every animal species. In addition to the basic fodder, the requirements of individual species call for the addition of Calcium carbonate to the feed ratio. The beneficial effect of using calcium carbonate as animal feed is proper development of bones and teeth, regulation of heartbeat and blood clotting, muscle contraction and nerve impulses, enzyme activation and hormone secretion, eggshell formation and quality, and milk production.</p>
                    </div>
                    <?php else: ?>
                    <div class="title-content">
                        <p>Pakan ternak</p>
                    </div>
                    <div class="isinya">
                        <p>Kalsium adalah unsur pencampuran penting untuk setiap spesies hewan. Selain pakan dasar, persyaratan spesies individu membutuhkan penambahan kalsium karbonat ke dalam rasio pakan. Efek menguntungkan dari menggunakan kalsium karbonat sebagai pakan ternak adalah pengembangan tulang dan gigi yang tepat, pengaturan detak jantung dan pembekuan darah, kontraksi otot dan impuls saraf, aktivasi enzim dan sekresi hormon, pembentukan dan kualitas kulit telur, pembentukan dan kualitas kulit telur, serta produksi susu.</p>
                    </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="col-md-60">
                <div class="content" id="fishery">
                    <?php if (Yii::app()->language == 'en'): ?>
                    <div class="title-content">
                        <p>Fishery</p>
                    </div>
                    <div class="isinya">
                        <p>The aim of good pond management is to increase fish production through an improved supply of natural food such as phytoplankton and zooplankton. The supply is usually increased by fertilizing the pond water. Addition of calcium carbonate at pond is called liming, liming can be done when the pH of the pond below 6.5, very muddy, controlling disease and pests, high concentration of organic matter, and total alkalinity below 25 mg/l CaCO3.</p>
                    </div>
                    <?php else: ?>
                    <div class="title-content">
                        <p>Perikanan</p>
                    </div>
                    <div class="isinya">
                        <p>Tujuan dari pengelolaan kolam yang baik adalah untuk meningkatkan produksi ikan melalui peningkatan pasokan makanan alami seperti fitoplankton dan zooplankton. Suplai biasanya meningkat dengan menyuburkan air tambak. Penambahan kalsium karbonat di tambak disebut pengapuran, pengapuran dapat dilakukan ketika pH kolam di bawah 6,5, sangat berlumpur, mengendalikan penyakit dan hama, konsentrasi tinggi bahan organik, dan alkalinitas total di bawah 25 mg / l CaCO3.</p>
                    </div>
                    <?php endif ?>
                    <div class="pb-5"></div>
                    <div class="pb-3"></div>
                    <div class="hr-produk"></div>
                </div>
            </div>
        </div>

        <div class="row wow fadeInDown">
            <div class="col-md-60">
                <div class="footer-produk-bawah">
                    <div class="pt-5"></div>
                    <div class="pt-3"></div>
                    <?php if (Yii::app()->language == 'en'): ?>
                    <h5>WE PRODUCE CALCIUM CARBONATES TO YOUR REQUIREMENTS</h5>
                    <h6>Please enquire - our consultant will discuss your detailed specifications (purity, mesh, etc).</h6>
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>"><p>CONTACT US</p></a>
                    <?php else: ?>
                    <h5>KAMI MENGHASILKAN KARBONAT KALSIUM UNTUK PERSYARATAN ANDA</h5>
                    <h6>Silakan tanyakan - konsultan kami akan membahas spesifikasi terperinci Anda (kemurnian, mesh, dll).</h6>
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>"><p>KONTAK</p></a>
                    <?php endif ?>
                </div>
            </div>
        </div>

        <?php /*<div class="row">
            <div class="col-md-60">
                <div class="title pt-5 mx-auto d-block text-center">
                    <?php if (Yii::app()->language == 'en'): ?>
                    <h3>our product variety</h3>
                    <?php else: ?>
                    <h3>VARIASI PRODUK KAMI</h3>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-20">
                <div class="box-produk pt-5">
                    <div class="img pt-3">
                        <img class="img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>design-1-products_03<?php echo $i ?>.jpg" alt="">
                    </div>
                    <div class="title pt-4">
                        <h3>Calcium Carbonate Type A</h3>
                    </div>
                    <div class="read">
                        <p>
                            <a href="#"><?php echo Tt::t('front', 'Read More'); ?></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-20">
                <div class="box-produk pt-5">
                    <div class="img pt-3">
                        <img class="img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>design-1-products_03<?php echo $i ?>.jpg" alt="">
                    </div>
                    <div class="title pt-4">
                        <h3>Calcium Carbonate Type B</h3>
                    </div>
                    <div class="read">
                        <p>
                            <a href="#"><?php echo Tt::t('front', 'Read More'); ?></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-20">
                <div class="box-produk pt-5">
                    <div class="img pt-3">
                        <img class="img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>design-1-products_03<?php echo $i ?>.jpg" alt="">
                    </div>
                    <div class="title pt-4">
                        <h3>Calcium Carbonate Type C</h3>
                    </div>
                    <div class="read">
                        <p>
                            <a href="#"><?php echo Tt::t('front', 'Read More'); ?></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-20">
                <div class="box-produk pt-5">
                    <div class="img pt-3">
                        <img class="img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>design-1-products_03<?php echo $i ?>.jpg" alt="">
                    </div>
                    <div class="title pt-4">
                        <h3>Calcium Carbonate Type A</h3>
                    </div>
                    <div class="read">
                        <p>
                            <a href="#"><?php echo Tt::t('front', 'Read More'); ?></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-20">
                <div class="box-produk pt-5">
                    <div class="img pt-3">
                        <img class="img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>design-1-products_03<?php echo $i ?>.jpg" alt="">
                    </div>
                    <div class="title pt-4">
                        <h3>Calcium Carbonate Type B</h3>
                    </div>
                    <div class="read">
                        <p>
                            <a href="#"><?php echo Tt::t('front', 'Read More'); ?></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-20">
                <div class="box-produk pt-5">
                    <div class="img pt-3">
                        <img class="img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>design-1-products_03<?php echo $i ?>.jpg" alt="">
                    </div>
                    <div class="title pt-4">
                        <h3>Calcium Carbonate Type C</h3>
                    </div>
                    <div class="read">
                        <p>
                            <a href="#"><?php echo Tt::t('front', 'Read More'); ?></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>*/ ?>
    </div>
    <div class="pb-5"></div>
    <div class="pb-5"></div>
    <!-- <div class="pb-5"></div> -->
</section>