<section class="home-sec-1">
	<div class="prelative container py-5">
		<div class="row py-3">
			<div class="col-md-30 wow fadeInLeft">
				<?php if (Yii::app()->language == 'en'): ?>
				<div class="box-content">
					<div class="title">
						<p>Calcium Carbonate that we manufacture...</p>
					</div>
					<div class="content pt-4">
						<p>We take pride in providing high quality calcium carbonate products to our market in Surabaya, Jakarta and throuhout Indonesia. We will meet your specification and we will ensure a steady production flow by maintaining our stock priority for your long-term order suply.</p>
					</div>
					<div class="link pt-4">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/produk', 'lang'=>Yii::app()->language)); ?>">
							<p>our calcium carbonate products</p>
						</a>
					</div>
				</div>
				<?php else: ?>
				<div class="box-content">
					<div class="title">
						<p>Kalsium Karbonat yang kami produksi...</p>
					</div>
					<div class="content pt-4">
						<p>Kami bangga menyediakan produk kalsium karbonat berkualitas tinggi untuk pasar kami di Surabaya, Jakarta dan di seluruh Indonesia. Kami akan memenuhi spesifikasi Anda dan kami akan memastikan aliran produksi yang stabil dengan mempertahankan prioritas stok kami untuk suply pesanan jangka panjang Anda.</p>
					</div>
					<div class="link pt-4">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/produk', 'lang'=>Yii::app()->language)); ?>">
							<p>produk kalsium karbonat kami</p>
						</a>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<div class="col-md-30 wow fadeInRight">
				<img class="img img-fluid" src="<?php echo $this->assetBaseurl; ?>design-1_03<?php echo $i ?>.jpg" alt="">
			</div>
		</div>
	</div>
</section>

<section class="home-sec-2" id="div1">
	<div class="py-3 hide-pc"></div>
	<div class="py-3"></div>
	<div class="py-3"></div>
	<div class="prelative container">
		<div class="title-sec wow fadeInUp">
			<?php if (Yii::app()->language == 'en'): ?>
			<p>More On Dwi Selo Giri Mas - The Calcium Carbonate Factory</p>
			<?php else: ?>
			<p>Tentang Dwi Selo Giri Mas - Pabrik Kalsium Karbonat</p>
			<?php endif; ?>
		</div>
		<div class="row">
			<div class="col-md-20 wow fadeInUp">
				<div class="box-content pt-4">
					<?php if (Yii::app()->language == 'en'): ?>
					<div class="title-box pb-2">
						<h1>About Us</h1>
					</div>
					<img class="w-100" src="<?php echo $this->assetBaseurl; ?>design-1_07<?php echo $i ?>.png" alt="">
					<div class="content py-4">
						<p>We make products that is essential for your factory to run smooth. We bring benefits in more ways without you even realizing it.</p>
					</div>
					<div class="link">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">
							<p>read more</p>
						</a> 
					</div>
					<?php else: ?>
					<div class="title-box pb-2">
						<h1>Tentang Kami</h1>
					</div>
					<img class="w-100" src="<?php echo $this->assetBaseurl; ?>design-1_07<?php echo $i ?>.png" alt="">
					<div class="content py-4">
						<p>Kami membuat produk yang penting bagi pabrik Anda untuk berjalan mulus. Kami membawa manfaat dengan lebih banyak cara tanpa Anda sadari.</p>
					</div>
					<div class="link">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">
							<p>lebih lanjut</p>
						</a> 
					</div>
					<?php endif; ?>
				</div>
			
			</div>
			<div class="col-md-20 wow fadeInDown">
				<div class="box-content pt-4">
					<?php if (Yii::app()->language == 'en'): ?>
					<div class="title-box pb-2">
						<h1>Process & Quality</h1>
					</div>
					<img class="w-100" src="<?php echo $this->assetBaseurl; ?>design-1_09<?php echo $i ?>.png" alt="">
					<div class="content py-4">
						<p>Our strict quality control is integrated and we offer full integrity and traceability for all our products.</p>
					</div>
					<div class="link">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/process', 'lang'=>Yii::app()->language)); ?>">
							<p>read more</p>
						</a> 
					</div>
					<?php else: ?>
					<div class="title-box pb-2">
						<h1>Proses & Kualitas</h1>
					</div>
					<img class="w-100" src="<?php echo $this->assetBaseurl; ?>design-1_09<?php echo $i ?>.png" alt="">
					<div class="content py-4">
						<p>Kontrol kualitas kami yang ketat terintegrasi dan kami menawarkan integritas penuh dan keterlacakan untuk semua produk kami.</p>
					</div>
					<div class="link">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/process', 'lang'=>Yii::app()->language)); ?>">
							<p>lebih lanjut</p>
						</a> 
					</div>
					<?php endif; ?>
				</div>

			</div>
			<div class="col-md-20 wow fadeInUp">
				<div class="box-content pt-4">
					<?php if (Yii::app()->language == 'en'): ?>
					<div class="title-box pb-2">
						<h1>Career</h1>
					</div>
					<img class="w-100" src="<?php echo $this->assetBaseurl; ?>design-1_11<?php echo $i ?>.png" alt="">
					<div class="content py-4">
						<p>Join us and become part of a growing national company with a culture that allows you to bring out the best in you.</p>
					</div>
					<div class="link">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/career', 'lang'=>Yii::app()->language)); ?>">
							<p>read more</p>
						</a> 
					</div>
					<?php else: ?>
					<div class="title-box pb-2">
						<h1>Karir</h1>
					</div>
					<img class="w-100" src="<?php echo $this->assetBaseurl; ?>design-1_11<?php echo $i ?>.png" alt="">
					<div class="content py-4">
						<p>Bergabunglah dengan kami dan menjadi bagian dari perusahaan nasional yang berkembang dengan budaya yang memungkinkan Anda untuk mengeluarkan yang terbaik dari diri Anda.</p>
					</div>
					<div class="link">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/career', 'lang'=>Yii::app()->language)); ?>">
							<p>lebih lanjut</p>
						</a> 
					</div>
					<?php endif; ?>
				</div>

			</div>
		</div>
	</div>
	<div class="py-3 hide-pc"></div>
	<div class="py-3"></div>
	<div class="py-3"></div>
</section>

<?php 
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('active = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->order = 'date_input DESC';
$criteria->limit = 3;

$news_hm = new CActiveDataProvider('Blog', array(
'criteria'=>$criteria,
'pagination'=>false,
));
?>
<?php if ( $news_hm->totalItemCount > 0): ?>
<section class="home-sec-3">
	<div class="py-3 hide-pc"></div>
	<div class="py-3"></div>
	<div class="py-3"></div>
	<div class="prelative container">
		<div class="title-sec pb-5 wow fadeInDown">
			<?php if (Yii::app()->language == 'en'): ?>
			<p>Latest Events About Calcium Carbonate</p>
			<?php else: ?>
			<p>Berita & Artikel Terbaru Tentang Kalsium Karbonat</p>
			<?php endif ?>
		</div>

		<div class="row">

			<?php foreach ($news_hm->getData() as $key => $value): ?>        
	        <div class="col-md-20">
	          <div class="box-content">
	            <div class="tanggal pb-4">
	              <p><?php echo date('d F Y', strtotime($value->date_input)); ?></p>
	            </div>
	            <div class="title pb-3">
	              <a href="<?php echo CHtml::normalizeUrl(array('/home/news_detail', 'id'=>$value->id, 'lang'=>Yii::app()->language)); ?>">
	                <h1><?php echo $value->description->title ?></h1>
	              </a>
	            </div>
	            <div class="content pb-4">
	              <p><?php echo substr(strip_tags($value->description->content), 0, 150); ?></p>
	            </div>
	          </div>
	        </div>
	        <?php endforeach ?>

		</div>
		<div class="more text-right">
			<a href="<?php echo CHtml::normalizeUrl(array('/home/news', 'lang'=>Yii::app()->language)); ?>">
				<?php if (Yii::app()->language == 'en'): ?>
				<p>More events</p>
				<?php else: ?>
				<p>Lebih banyak berita & artikel</p>
				<?php endif ?>
			</a>
		</div>
	</div>
	<div class="py-3 hide-pc"></div>
	<div class="py-3"></div>
	<div class="py-3"></div>
</section>
<?php endif ?>
