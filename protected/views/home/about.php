<section class="cover-about page-about">
  <div class="container cont-fcs produk">
    <div class="cover-image">
        <!-- <img class="w-100 d-block" src="<?php echo $this->assetBaseurl; ?>ill-about.jpg" alt=""> -->
        <div class="centered wow fadeInUp">
          <p>
            <?php echo strtoupper( Tt::t('front', 'About Us') ); ?>
          </p>
        </div>
    </div>
    <div class="row pt-3">
      <div class="col-40">
        <div class="breadcrumb wow fadeInUp">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">HOME</a></li>
              <li class="breadcrumb-item"><a href="#"><?php echo strtoupper( Tt::t('front', 'About Us') ); ?></a></li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="col-20">
        <div class="back text-right wow fadeInUp">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><?php echo Tt::t('front', 'BACK'); ?></a>
        </div>
      </div>
    </div>
    <hr class="cover">
  </div>
  <div class="prelative container pt-5 pt-1">
    <div class="row">
      <div class="col-md-30">
        <div class="image wow fadeInUp">
          <img class="img img-fluid" src="<?php echo $this->assetBaseurl; ?>design-1-about_03<?php echo $i ?>.jpg" alt="">
        </div>
      </div>
      <div class="col-md-30">
        <div class="caption wow fadeInUp">
          <?php if (Yii::app()->language == 'en'): ?>
          <div class="text1 pt-2 pb-4">
            <p>WHO WE ARE</p>
          </div>
          <div class="text2 pb-4">
            <p>PT. DWI SELO GIRI MAS THE CALCIUM CARBONATE MANUFACTURER</p>
          </div>
          <div class="text3">
            <p>Dwi Selo Giri Mas (DSGM) is an established manufacturer - supplier of Calcium Carbonate from Sidoarjo - Surabaya, East Java. DSGM has a proven track record as a calcium carbonate manufacturer since 1988. Our market for customers spread across the Java (Jakarta, Surabaya, Gresik, Mojokerto, and more), and even through out to islands such as Kalimantan and Sulawesi. We have a refreshingly innovative mix of state of the art machinery plus a qualified and experienced team that serves with you in mind. DSGM is committed to providing customers with quality products that are delivered on time, our focus and commitment is always on your success. </p>
          </div>
          <?php else: ?>
          <div class="text1 pt-2 pb-4">
            <p>SIAPA KITA</p>
          </div>
          <div class="text2 pb-4">
            <p>PT. DWI SELO GIRI MAS PRODUSEN KALSIUM KARBONAT</p>
          </div>
          <div class="text3">
            <p>Dwi Selo Giri Mas (DSGM) adalah produsen - pemasok Kalsium Karbonat yang mapan dari Sidoarjo - Surabaya, Jawa Timur. DSGM memiliki rekam jejak yang terbukti sebagai produsen kalsium karbonat sejak tahun 1988. Pasar kami untuk pelanggan tersebar di seluruh Jawa (Jakarta, Surabaya, Gresik, Mojokerto, dan banyak lagi), dan bahkan hingga ke pulau-pulau seperti Kalimantan dan Sulawesi. Kami memiliki perpaduan yang inovatif dan canggih dari mesin-mesin canggih plus tim yang berkualitas dan berpengalaman yang melayani Anda dalam pikiran. DSGM berkomitmen untuk menyediakan pelanggan dengan produk-produk berkualitas yang dikirimkan tepat waktu, fokus dan komitmen kami selalu pada kesuksesan Anda.</p>
          </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="about-sec-1">
  <div class="pt-5 hide-pc"></div>
	<div class="prelative container py-5">
		<div class="row py-3 wow fadeInDown">
			<div class="col-md-30">
				<div class="box-content">
          <?php if (Yii::app()->language == 'en'): ?>
          <div class="what pb-4">
            <p>WHy us</p>
          </div>
					<div class="title">
						<p>Our experience and reputation is one of the best in the field.</p>
					</div>
					<div class="content pt-4">
						<p class="mb-4">Dwi Selo Giri Mas (DSGM) has designed flexibility into our process, this allows DSGM to cater to all customers specific requirement to produce the optimal calcium carbonate powder as required. Being well known for our reputation since 1988 helps us to source the best raw materials and respond quickly to customer's needs. The efficient running investment on quality manufacturing enables us to deliver our calcium carbonate products at a price to beat.</p>
            <p class="mb-4">The key of our product success lies on our thight quality control and our strict production flow protocols. Feel free to inquire your needs of calcium carbonate products.</p>
            <p class="mb-4">Our solid partnerships are formed and strengthened due to the values, purpose and vision that our employees live by. Our core purpose is to keep businesses running and that is backed by our values of get better every day, love they neighbor, do what it takes and own your future. The combination of our purpose and values and the quality of our products, makes us a strong supplier; your supplier of choice and where you can buy calcium carbonate powder.</p>
					</div>
					<div class="link pt-2">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/produk', 'lang'=>Yii::app()->language)); ?>">
							<p>VIEW OUR CALCIUM CARBONATE PRODUCTS</p>
						</a>
					</div>
          <?php else: ?>
          <div class="what pb-4">
            <p>Mengapa kita</p>
          </div>
          <div class="title">
            <p>Pengalaman dan reputasi kami adalah salah satu yang terbaik di bidangnya.</p>
          </div>
          <div class="content pt-4">
            <p class="mb-4">Dwi Selo Giri Mas (DSGM) telah merancang fleksibilitas dalam proses kami, ini memungkinkan DSGM untuk memenuhi semua kebutuhan khusus pelanggan untuk menghasilkan bubuk kalsium karbonat yang optimal sesuai kebutuhan. Menjadi terkenal karena reputasi kita sejak tahun 1988 membantu kita untuk sumber bahan baku terbaik dan cepat menanggapi kebutuhan pelanggan. Investasi berjalan yang efisien pada pembuatan yang berkualitas memungkinkan kami untuk mengirimkan produk-produk kalsium karbonat dengan harga yang dapat dikalahkan.</p>
            <p class="mb-4">Kunci kesuksesan produk kami terletak pada kontrol kualitas paha Anda dan protokol aliran produksi kami yang ketat. Jangan ragu untuk menanyakan kebutuhan Anda akan produk kalsium karbonat.</p>
            <p class="mb-4">Kemitraan yang solid kami terbentuk dan diperkuat karena nilai-nilai, tujuan, dan visi yang dijalani karyawan kami. Tujuan utama kami adalah menjaga bisnis tetap berjalan dan itu didukung oleh nilai-nilai kami untuk menjadi lebih baik setiap hari, mencintai sesamanya, melakukan apa yang diperlukan dan memiliki masa depan Anda. Kombinasi tujuan dan nilai-nilai kami dan kualitas produk kami, menjadikan kami pemasok yang kuat; pemasok pilihan Anda dan di mana Anda dapat membeli bubuk kalsium karbonat.</p>
          </div>
          <div class="link pt-2">
            <a href="<?php echo CHtml::normalizeUrl(array('/home/produk', 'lang'=>Yii::app()->language)); ?>">
              <p>LIHAT PRODUK KARBONAT KALSIUM KAMI</p>
            </a>
          </div>
          <?php endif; ?>
				</div>
			</div>
			<div class="col-md-30">
				<img class="img img-fluid" src="<?php echo $this->assetBaseurl; ?>design-1-about_07<?php echo $i ?>.jpg" alt="">
			</div>
		</div>
	</div>
</section>
<div class="pb-5"></div>
<div class="pb-4 hide-pc"></div>
<div class="pb-5 hide-pc"></div>

<section class="about-sec-2">
  <div class="row no-gutters wow fadeInUp">
    <div class="col-md-60">
      <img src="<?php echo $this->assetBaseurl; ?>fulls_about_comp.jpg" alt="" class="img img-fluid w-100">
    </div>
  </div>
  <div class="row no-gutters wow fadeInUp">
    <div class="col-md-30">
      <div class="back-left py-5">
        <div class="box-vm py-5">
          <div class="vision-mission">
            <?php if (Yii::app()->language == 'en'): ?>
            <p>Vision</p>
            <?php else: ?>
            <p>Visi</p>
            <?php endif; ?>
          </div>
          <div class="caption pt-4">
            <?php if (Yii::app()->language == 'en'): ?>
            <p>To be the trusted leader in calcium carbonate industry that surpass our customer's needs</p>
            <?php else: ?>
            <p>Untuk menjadi produsen produk Kalsium Karbonat terkemuka dan terkemuka di Indonesia dan memperluas jangkauan pasar kami ke dunia.</p>
            <?php endif; ?>
            <div class="d-none d-sm-block">
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-30">
      <div class="back-right py-5">
        <div class="box-vm py-5">
          <div class="vision-mission">
            <?php if (Yii::app()->language == 'en'): ?>
            <p>Mission</p>
            <?php else: ?>
            <p>Misi</p>
            <?php endif; ?>
          </div>
          <div class="caption pt-4">
            <?php if (Yii::app()->language == 'en'): ?>
            <ul>
              <li>We supply high quality of calcium carbonate while stimulating the growth of our communities – team members, customers shareholders, local society and environment.
              </li>
              <li>It is the fundamental value upon we conduct our business and relationships with others. We are honest, open, fair, trustworthy and ethical.
              </li>
              <li>We value our customers and we strive to satisfy their expectations by delivering superior products, services and solutions.
              </li>
              <li>We value every team member for their needs and safety. We treat each other with respect and openness as if they are our own family.
              </li>
              <li>We strive to minimize our impact on the environment while we increase our contribution to the local society.
              </li>
            </ul>
            <?php else: ?>
            <p>Memenuhi semua kebutuhan pelanggan kami, dan memberikannya dengan nilai tambah untuk menciptakan hubungan obligasi jangka panjang.</p>
            <p>Untuk menghadirkan pengalaman penjualan dan layanan yang lebih baik yang membuat kami berbeda dari yang lain.</p>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<style type="text/css">
  section.about-sec-1 .box-content .content p{
    max-width: 450px;
    text-align: justify;
  }
</style>