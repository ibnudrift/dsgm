<section class="home-sec-4">
	<div class="prelative container pt-5">
		<div class="row">
			<div class="col-md-40">
				<div class="box-section pt-5 wow fadeInDown">
					<?php if (Yii::app()->language == 'en'): ?>
					<div class="title pt-5">
						<p>Contact us for inquiry</p>
					</div>
					<div class="subtitle pt-4 pb-5">
						<p>Our representative staff will respond to you son to discuss so many probabilities and solutions that you may require.</p>
					</div>
					<?php else: ?>
					<div class="title pt-5">
						<p>Kami ingin mengenal Anda...</p>
					</div>
					<div class="subtitle pt-4 pb-5">
						<p>Staf perwakilan kami akan menanggapi Anda untuk mendiskusikan begitu banyak probabilitas dan solusi yang mungkin Anda perlukan.</p>
					</div>
					<?php endif; ?>
					<?php 
					$model = new ContactForm;
					$model->scenario = 'insert';

					if(isset($_POST['ContactForm']))
					{
						$model->attributes=$_POST['ContactForm'];
						$status = true;
				        $secret_key = "6LeAzqgUAAAAAIiSaUlGDCQbivxJadFquA_9OfcX";

						$url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR'];
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL, $url);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
						curl_setopt($curl, CURLOPT_TIMEOUT, 15);
						curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE);
						curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, TRUE); 
						$curlData = curl_exec($curl);
						curl_close($curl);
				        $response = json_decode($curlData);

				        if($response->success==false)
				        {
				          $status = false;
				          $model->addError('verifyCode', 'Pastikan anda sudah menyelesaikan captcha');
				        }

						if($status AND $model->validate())
						{
							// config email
							$messaged = $this->renderPartial('//mail/enquire',array(
								'model'=>$model,
							),TRUE);

							$config = array(
								'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email'], 'ibnudrift@gmail.com'),
								'subject'=>'['.Yii::app()->name.'] Enquire Contact from '.$model->email,
								'message'=>$messaged,
							);
							if ($this->setting['contact_cc']) {
								$config['cc'] = array($this->setting['contact_cc']);
							}
							if ($this->setting['contact_bcc']) {
								$config['bcc'] = array($this->setting['contact_bcc']);
							}
							
							// echo "<pre>";
							// print_r($config);
							// exit;

							// kirim email
							Common::mail($config);

							Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
							$this->refresh();
						}

					}
					?>

					<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
				                'type'=>'',
				                'enableAjaxValidation'=>false,
				                'clientOptions'=>array(
				                    'validateOnSubmit'=>false,
				                ),
				                'htmlOptions' => array(
				                    'enctype' => 'multipart/form-data',
				                    'class'=>'py-5',
				                ),
				            )); ?>
				    <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>  
				    <?php if(Yii::app()->user->hasFlash('success')): ?>
				        <?php /*$this->widget('bootstrap.widgets.TbAlert', array(
				            'alerts'=>array('success'),
				        ));*/ ?>
				        <script type="text/javascript">
                        $(function(){
                           $('#call_modal').click(); 
                        });
                        </script>
				    <?php endif; ?>
						<div class="row default">
							<div class="col-md-27 col-sm-27">
								<div class="form-group">
									<?php echo $form->textField($model, 'name', array('class'=>'form-control', 'placeholder'=>'Name')); ?>
								</div>
							</div>
							<div class="col-md-27 col-sm-27">
								<div class="form-group">
									<?php echo $form->textField($model, 'company', array('class'=>'form-control', 'placeholder'=>'Company')); ?>
								</div>
							</div>
						</div>

						<div class="row default">
							<div class="col-md-27 col-sm-27">
								<div class="form-group">
									<?php echo $form->textField($model, 'email', array('class'=>'form-control', 'placeholder'=>'Email')); ?>
								</div>
							</div>
							<div class="col-md-27 col-sm-27">
								<div class="form-group">
								<?php echo $form->textField($model, 'phone', array('class'=>'form-control', 'placeholder'=>'Phone')); ?>
								</div>
							</div>
						</div>

						<div class="row default">
						<div class="col-md-60">
							<div class="form-group">
						</div>
						<div class="clear"></div>

						<div class="col-md-60">
							<div class="row default">
								<div class="col-md-27 col-sm-27">
									<div class="d-block picts_captcha" style="margin-left: -15px;">
							            <div class="g-recaptcha" data-sitekey="6LeAzqgUAAAAAHQABMAYvPm5BOrhCKm6WzaQHl6S"></div>
							          </div>
								</div>
								<div class="col-md-27 col-sm-27 hide-pc">
									<div class="text-right submit">
									<button type="submit">Submit</button>
									</div>
								</div>
								<div class="col-md-60 hide-mobile">
									<div class="text-center submit">
									<button type="submit">Submit</button>
									</div>
								</div>
															
								<div class="py-5 hide-mobile"></div>
								<div class="py-5 hide-mobile"></div>
							</div>

						</div>
						</div>
					<?php $this->endWidget(); ?>
				</div>
			</div>
			<div class="col-md-20">
				
			</div>
		</div>
	</div>
</section>

<div class="pt-5"></div>
<div class="pt-3"></div>
<section class="footer">
    <div class="prelative container wow fadeInUp">
        <div class="row">
            <div class="col-md-60 wow fadeInUp">
                <div class="no-telp text-center">
                    <p>Call Our Hotline <span><img src="<?php echo $this->assetBaseurl; ?>phone-copy.png" alt=""></span><b>031-8913030</b>  or <a href="tel:+62318913030">click here </a>to contact us.</p>
                </div>
            </div>
            <div class="col-md-60 wow fadeInUp">
                <div class="gambar text-center pt-5">
                    <img src="<?php echo $this->assetBaseurl; ?>logo-footer<?php echo $i ?>.png" alt="">
                </div>
            </div>
            <div class="col-md-60 text-center wow fadeInUp">
                <div class="bawah-logo pt-4">
                    <p>PT. DWI SELO GIRI MAS</p>
                </div>
            </div>
            <div class="col-md-60 text-center wow fadeInUp">
                <div class="copyright pt-5">
                    <p>Copyright 2019 &copy; PT. Dwi Selo Giri Mas - Lokasi Manufacturer in Gedangan - Sidoarjo, Indonesia. All rights reserved.</p>
                </div>
            </div>
            <div class="col-md-60 text-center wow fadeInUp">
                <div class="website-design pt-1 pb-5">
                    <p>Website design by <a target="_blank" title="Website Design Surabaya" href="https://www.markdesign.net/">Mark Design Indonesia</a>.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
	section.home-sec-4 .box-section form button{
		cursor: pointer;
	    background: 0 0;
	    border: 0;
	    padding: 9px 25px 9px 150px;
	    color: #fff;
	    text-transform: uppercase;
	    font-size: 18px;
	    font-family: Montserrat,sans-serif;
	    letter-spacing: 1px;
	    text-decoration: none;
	}
	.alert,
	.alert.alert-danger{
		font-size: 13px;
	}
	.alert ul,
	.alert.alert-danger ul{
		margin-bottom: 0;
	}
	
	.modal-dialog {
        min-height: calc(100vh - 60px);
        display: flex;
        flex-direction: column;
        justify-content: center;
        overflow: auto;
    }
    @media(max-width: 768px) {
      .modal-dialog {
        min-height: calc(100vh - 20px);
      }
    }
</style>
<script src='https://www.google.com/recaptcha/api.js'></script>

<button id="call_modal" type="button" data-toggle="modal" data-target="#myModal" style="visibility: hidden;">Launch modal</button>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Thank You</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Thank you for contact us. We will respond to you as soon as possible.</p>
      </div>
    </div>
  </div>
</div>