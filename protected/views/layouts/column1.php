<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>


<!-- Start fcs -->
<?php
    $criteria=new CDbCriteria;
    $criteria->with = array('description');
    $criteria->addCondition('description.language_id = :language_id');
    $criteria->addCondition('active = 1');
    $criteria->params[':language_id'] = $this->languageID;
    $criteria->group = 't.id';
    $criteria->order = 't.id ASC';
    $slide = Slide::model()->with(array('description'))->findAll($criteria);

?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide">
    <div class="container cont-fcs">
        <div id="myCarousel_home" class="carousel slide" data-ride="carousel" data-interval="6000" data-touch="true">
                <div class="carousel-inner">
                    <?php foreach ($slide as $key => $value): ?>
                    <div class="carousel-item <?php if($key == 0): ?>active<?php endif ?> home-slider-new">
                        <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1834,756, '/images/slide/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="First slide">
                        <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(774,867, '/images/slide/'. $value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
                    </div>
                    <?php endforeach; ?>
                </div>
                <ol class="carousel-indicators">
                    <?php foreach ($slide as $key => $value): ?>
                    <li data-target="#myCarousel_home" data-slide-to="<?php echo $key ?>" <?php if ($key == 0): ?>class="active"<?php endif ?>>></li>
                    <?php endforeach; ?>
                </ol>
                <div class="carousel-caption caption-slider-home mx-auto">
                    <div class="prelatife container mx-auto">
                        <div class="bxsl_tx_fcs">
                            <div class="row no-gutters">
                                <div class="col-md-10"></div>
                                <div class="col-md-40">
                                    <div class="text-slide wow fadeInUp">
                                        <?php if (Yii::app()->language == 'en'): ?>
                                        <p>As a reliable quality calcium carbonate manufacturer,
                                            the only key that drives our company's engine is 
                                        </p>
                                        <h5>our focus & comitment <strong>on your success.</strong></h5>
                                        <?php else: ?>
                                        <p>Sebagai produsen kalsium karbonat berkualitas yang dapat diandalkan, satu-satunya kunci yang menggerakkan mesin perusahaan kami adalah
                                        </p>
                                        <h5>FOKUS DAN KOMITMEN KAMI PADA <b>KEBERHASILAN ANDA</b>.</h5>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-10"></div>
                            </div>
                        </div>
                        <div class="clear clearfix"></div>
                    </div>
                </div>
        </div>

    </div>
</div>

<section class="above-slide">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60 wow fadeInUp">
                <div class="box-scroll">
                    <div class="pt-5"></div>
                    <div class="pt-4"></div>
                    <?php if (Yii::app()->language == 'en'): ?>
                    <h4>How we’ve become essential part of your business.</h4>
                    <div class="pt-1 pb-5">
                    <p>We make calcium carbonate that you can rely and you can count on. Without you even realizing it, we make your operation run smooth and put your product at a high quality stage.  It’s our legacy of over decades since 1988, built and nurtured on the solid foundation of doing business with ethics and values of partnership.</p>
                    <?php else: ?>
                    <h4>Bagaimana kami menjadi bagian penting dari bisnis Anda.</h4>
                    <div class="pt-1 pb-5">
                    <p>Kami membuat kalsium karbonat yang dapat Anda andalkan dan dapat Anda andalkan. Tanpa Anda sadari, kami membuat operasi Anda berjalan mulus dan menempatkan produk Anda pada tahap kualitas tinggi. Ini adalah warisan kami selama puluhan tahun sejak tahun 1988, dibangun dan dipelihara di atas dasar yang kuat untuk melakukan bisnis dengan etika dan nilai-nilai kemitraan.</p>
                    <?php endif; ?>
                    </div>
                    <a href="#">
                        <img id="click" src="<?php echo $this->assetBaseurl; ?>scroll-down<?php echo $i ?>.png" alt="">
                    </a>
                    <div class="pb-5 hide-pc"></div>
                    <div class="pb-5"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End fcs -->

<?php echo $content ?>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>

<?php echo $this->renderPartial('//layouts/_swipe_mobile', array()); ?>

<script type="text/javascript">
    $(document).ready(function(){
        
        // if ($(window).width() >= 768) {
        //     var $item = $('#myCarousel_home.carousel .carousel-item'); 
        //     var $wHeight = $(window).height();
        //     $item.eq(0).addClass('active');
        //     $item.height($wHeight); 
        //     $item.addClass('full-screen');

        //     $('#myCarousel_home.carousel img.d-none.d-sm-block').each(function() {
        //       var $src = $(this).attr('src');
        //       var $color = $(this).attr('data-color');
        //       $(this).parent().css({
        //         'background-image' : 'url(' + $src + ')',
        //         'background-color' : $color
        //       });
        //       $(this).remove();
        //     });

        //     $(window).on('resize', function (){
        //       $wHeight = $(window).height();
        //       $item.height($wHeight);
        //     });

        //     $('#myCarousel_home.carousel').carousel({
        //       interval: 4000,
        //       pause: "false"
        //     });
        // }

    });
</script>

<script type="text/javascript">
    $(document).ready(function (){
        $("#click").click(function (){
            $('html, body').animate({
                scrollTop: $("#div1").offset().top
            }, 2000);
        });
    });
</script>

<?php 
/*<script src="<?php echo Yii::app()->baseUrl; ?>/asset/js/textillate/jquery.fittext.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/asset/js/textillate/jquery.lettering.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/asset/js/textillate/jquery.textillate.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('.tlt').textillate({
            minDisplayTime: 650,
            in: { effect: 'fadeInUp', delay: 30, },
            out: { effect: 'pulse', sync: true, delay: 30 },
            loop: true,
        });

    });
</script>
*/
?>
<?php $this->endContent(); ?>
